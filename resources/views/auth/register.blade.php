@extends('app')

@section('title','Registro');

@section('css')

    <link rel="stylesheet" href="{{asset('css/mainAuth.css')}}">
    <link rel="stylesheet" href="{{asset('css/utilAutch.css')}}">

@endsection


@section('main')

    
        
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form method="POST" action="{{ route('register') }}" class="login100-form validate-form" >
                    @csrf
                    <span class="login100-form-title p-b-34">
                        Registrate es Gratis
                    </span>
                    
                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-20" data-validate="Su Usuario">
                        <input id="name" class="input100" type="text" name="name" placeholder="Nombres" autocomplete="off">
                        <span class="focus-input100"></span>
                       
                    </div>

                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-20" data-validate="Email" autocomplete="off">
                        <input class="input100" type="email" name="email"  id="email" placeholder="Email" >
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-20" data-validate="Password" autocomplete="off">
                        <input class="input100" type="password" name="password"  id="password" placeholder="Password" >
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-20" data-validate="Password" autocomplete="off">
                        <input class="input100" id="password-confirm" type="password" name="password_confirmation" required placeholder="Confirmar Password" autocomplete="new-password">
                        <span class="focus-input100"></span>
                    </div>

                    
                    
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Registrate
                        </button>
                    </div>

                    <div class="w-full text-center p-t-27 p-b-239">
                        <span class="txt1">
                            Forgot
                        </span>

                        <a href="#" class="txt2">
                            User name / password?
                        </a>
                    </div>

                    <div class="w-full text-center">
                        <a href="{{route('login')}}" class="txt3">
                            Login
                        </a>
                    </div>
                </form>

                <div class="login100-more img-register"></div>
            </div>
        </div>
    </div>

@endsection
