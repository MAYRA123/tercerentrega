@extends('app')

@section('title','Login')

@section('css')

    <link rel="stylesheet" href="{{asset('css/mainAuth.css')}}">
    <link rel="stylesheet" href="{{asset('css/utilAutch.css')}}">

@endsection

@section('main')


    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form method="POST" action="{{ route('login') }}" class="login100-form validate-form" >
                    @csrf
                    <span class="login100-form-title p-b-34">
                        Identifiquese para ingresar
                    </span>
                    
                    <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
                        <input id="email" class="input100" type="email" name="email" placeholder="Correo Electronico" autocomplete="off">
                        <span class="focus-input100"></span>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Password" autocomplete="off">
                        <input class="input100" type="password" name="password"  id="password" placeholder="Password" >
                        <span class="focus-input100"></span>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Inicio de Sesion
                        </button>
                    </div>

                    <div class="w-full text-center p-t-27 p-b-239">
                        <span class="txt1">
                            Forgot
                        </span>

                        <a href="#" class="txt2">
                            User name / password?
                        </a>
                    </div>

                    <div class="w-full text-center">
                        <a href="{{route('register')}}" class="txt3">
                            REGISTRATE
                        </a>
                    </div>
                </form>

                <div class="login100-more img-login"></div>
            </div>
        </div>
    </div>









@endsection
