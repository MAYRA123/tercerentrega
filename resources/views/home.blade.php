@extends('app')

@section('title','Formulario')


@section('css')
    
    <style>
        .menu-main li a{
            color:black !important;
        }
    </style>

@endsection

@section('main')

    <div class="container" style="transform:translateY(80px)">
        <form action="{{route('create')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12 text-center mb-5">
                    <h2 class="text-info font-weight-bold">Formulario de Registro e Inscripción</h2>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="carnet" placeholder="Carnet de Identidad">
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="gestion" placeholder="Gestion">
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="matricula" placeholder="Matricula">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="carrera" placeholder="Carrera">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nivel" placeholder="Nivel">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="grupo" placeholder="Grupo">
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="paterno" placeholder="Paterno">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="materno" placeholder="Materno">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nombres" placeholder="Nombres">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="date" class="form-control" name="nacimiento" placeholder="Nacimiento">
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="calle" placeholder="Calle y N°">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="zona" placeholder="Zona">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="telefono" placeholder="Telefono">
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" class="form-control" name="apoderado" placeholder="Apellidos y Nombres del Apoderado">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-info">Estado Civil</p>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" value="soltero" class="custom-control-input" id="soltero" name="estado">
                                <label class="custom-control-label" for="soltero">Soltero</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" value="casado" class="custom-control-input" id="casado" name="estado">
                                <label class="custom-control-label" for="casado">Casado</label>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-info">Genero</p>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" value="femenino" id="femenino" name="genero">
                                <label class="custom-control-label" for="femenino">Femenino</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" value="masculino" id="masculino" name="genero">
                                <label class="custom-control-label" for="masculino">Masculino</label>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <button type="submit" class="btn btn-primary mt-3">Crear Registro</button>
        
            @if(session()->has('bien'))
                <div class="alert alert-success mt-2">
                    {{Session::get('bien')}}
                </div>
            @endif

            @if($errors->any())
                <div class="alert alert-danger mt-3">
                    Ups existe errores en el formulario revise
                </div>    
            @endif

            <div class="row">
                <div class="col-md-12 mt-2">
                    <a href="{{route('listado')}}">Ver Listado de Usuarios</a>
                </div>
                
            </div>

            
            

        </form>
    </div>

@endsection
