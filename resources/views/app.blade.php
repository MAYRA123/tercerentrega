<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/styleMain.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
    @yield('css')
</head>
<body>
    
    <header>
        <ul class="menu-main">
            @auth
                <li class="">
                    <a class="">Principal</a>
                </li>
            @endauth
            <li class="">
                <a  class=""><b></b></a>
            </li>
            @auth
                <li class="">
                    <a href="{{route('salir')}}">Cerrar Session</a>
                </li>       
            @endauth      
            <li class=""><a class="" href="#about">About</a></li>
            <li class=""><a class="" href="#projects">Projects</a></li>
            <li class=""><a class="" href="#signup">Contact</a></li>
            @guest
                <li class="">
                    <a href="{{route('login')}}">Inicio de Sesion </a>
                </li>
            @endguest
            @guest
                <li class="">
                    <a  href="{{route('register')}}">Registro</a>
                </li>
            @endguest
          </ul>
    </header>
    <main id="app">
        @yield('main')
    </main>
    <footer>

    </footer>

    
    <script src="{{mix('js/app.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>