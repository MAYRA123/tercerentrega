@extends('app')

@section('title','FIND')


@section('css')
    
    <style>
        .menu-main li a{
            color:black !important;
        }
    </style>

@endsection

@section('main')

    <div class="container" style="transform:translateY(80px)">
        <form action="#" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12 text-center mb-5">
                    <h2 class="text-info font-weight-bold">Datos de {{$find->nombres}}</h2>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Carnet
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->carnet}}" name="carnet" placeholder="Carnet de Identidad">
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Gestion
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->gestion}}" name="gestion" placeholder="Gestion">
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Matricula
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->matricula}}" name="matricula" placeholder="Matricula">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Carrera
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->carrera}}" name="carrera" placeholder="Carrera">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Nivel
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->nivel}}" name="nivel" placeholder="Nivel">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Grupo
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->grupo}}" name="grupo" placeholder="Grupo">
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                A.Paterno
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->apellido_paterno}}"  name="paterno" placeholder="Paterno">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                A.materno
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->apellido_materno}}" name="materno" placeholder="Materno">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Nombres
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->nombres}}" name="nombres" placeholder="Nombres">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">
                            
                                Nacimiento
                            
                        </label>
                        <input type="date" class="form-control" value="{{$find->nacimiento}}" name="nacimiento" placeholder="Nacimiento">
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">
                            
                                Calle
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->calle}}" name="calle" placeholder="Calle y N°">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">
                           
                                Zona
                          
                        </label>
                        <input type="text" class="form-control" value="{{$find->zona}}" name="zona" placeholder="Zona">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">
                            
                                Telefono
                           
                        </label>
                        <input type="text" class="form-control" value="{{$find->telefono}}" name="telefono" placeholder="Telefono">
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">
                            
                                N.Apoderados
                            
                        </label>
                        <input type="text" class="form-control" value="{{$find->nombres_apoderado}}" name="apoderado" placeholder="Apellidos y Nombres del Apoderado">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-info">Estado Civil</p>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" {{$find->estado == 'soltero' ? 'checked' : ''}} value="soltero" class="custom-control-input" id="soltero" name="estado">
                                <label class="custom-control-label" for="soltero">Soltero</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" {{$find->estado == 'casado' ? 'checked' : ''}} value="casado" class="custom-control-input" id="casado" name="estado">
                                <label class="custom-control-label" for="casado">Casado</label>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-info">Genero</p>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" {{$find->genero == 'femenino' ? 'checked' : ''}} class="custom-control-input" value="femenino" id="femenino" name="genero">
                                <label class="custom-control-label" for="femenino">Femenino</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" {{$find->genero == 'masculino' ? 'checked' : ''}} class="custom-control-input" value="masculino" id="masculino" name="genero">
                                <label class="custom-control-label" for="masculino">Masculino</label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        
         

            <div class="row">
                <div class="col-md-12 mt-2">
                    <a href="{{route('listado')}}">Ver Listado de Usuarios</a>
                </div>
                <div class="col-md-12 mt-2 mb-3">
                    <button type="button" onclick="window.print();" class="btn btn-primary">
                        Imprimir Registros
                    </button>
                </div>
            </div>

            

            
            

        </form>
    </div>

@endsection
