@extends('app')

@section('title','Listado')


@section('css')
    
    <style>
        .menu-main li a{
            color:black !important;
        }
    </style>

@endsection

@section('main')


    <div class="container-fluid" style="transform:translateY(80px)">
        <div class="table-responsive p-2">
            <table class="table table-hover table-striped small">
                <thead>
                    <tr>
                        <th>Carnet</th>
                        <th>Gestion</th>
                        <th>Matricula</th>
                        <th>Carrera</th>
                        <th>Nivel</th>
                        <th>Grupo</th>
                        <th>A.Materno</th>
                        <th>A.Paterno</th>
                        <th>Nombres</th>
                        <th>Naciminto</th>
                        <th>Calle</th>
                        <th>Zona</th>
                        <th>Telefono</th>
                        <th>Apoderado</th>
                        <th>Estado</th>
                        <th>Genero</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($listado as $item)

                    <tr>

                       
                      
                            <td>{{$item->carnet}}</td>
                            <td>{{$item->gestion}}</td>
                            <td>{{$item->matricula}}</td>
                            <td>{{$item->carrera}}</td>
                            <td>{{$item->nivel}}</td>
                            <td>{{$item->grupo}}</td>
                            <td>{{$item->apellido_materno}}</td>
                            <td>{{$item->apellido_paterno}}</td>
                            <td>{{$item->nombres}}</td>
                            <td>{{$item->nacimiento}}</td>
                            <td>{{$item->calle}}</td>
                            <td>{{$item->zona}}</td>
                            <td>{{$item->telefono}}</td>
                            <td>{{$item->nombres_apoderado}}</td>
                            <td>{{$item->estado}}</td>
                            <td>{{$item->genero}}</td>
                            <td>
                                <a href="{{route('formulario.find',$item->id)}}">Ver Registro</a>
                            </td>

    
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12 mt-2 mb-3">
                {{$listado->links('pagination::bootstrap-4')}}
            </div>
            <div class="col-md-12 mt-2 mb-3">
                <button onclick="window.print();" class="btn btn-primary">
                    Imprimir Registros
                </button>
            </div>
            <div class="col-md-12 mt-2">
                <a href="{{route('home')}}">Registrar Nueva Matricula</a>
            </div>
        </div>

    </div>

@endsection
