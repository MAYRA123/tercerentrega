require('./bootstrap');

let app = new Vue({
    el:'#app',
    data:{

    },
    methods:{

    },
    mounted(){

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        
    }
});