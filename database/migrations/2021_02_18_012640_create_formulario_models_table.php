<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormularioModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulario', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('carnet');
            $table->text('gestion');
            $table->bigInteger('matricula');
            $table->text('carrera');
            $table->text('nivel');
            $table->text('grupo');
            
            $table->text('apellido_materno');
            $table->text('apellido_paterno');
            $table->text('nombres');

            $table->date('nacimiento');

            $table->text('calle');
            $table->text('zona');
            $table->text('telefono');

            $table->text('nombres_apoderado');

            $table->enum('estado',[
                'soltero',
                'casado',
            ]);

            $table->enum('genero',[
                'femenino',
                'masculino',
                'otro',
            ]);


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulario');
    }
}
