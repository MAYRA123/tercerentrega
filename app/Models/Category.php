<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function posts(){
        //devolveme todas las publicaciones con esta categoria
        return $this->hasMany(Post::class);
    }

    public function getRouterKeyName(){//id->slug
        return 'slug';
    }
}
