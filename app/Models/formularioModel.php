<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class formularioModel extends Model
{
    use HasFactory,SoftDeletes;


    protected $table = 'formulario';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'carnet',
        'gestion',
        'matricula',
        'carrera',
        'nivel',
        'grupo',
        'apellido_materno',
        'apellido_paterno',
        'nombres',
        'nacimiento',
        'calle',
        'zona',
        'telefono',
        'nombres_apoderado',
        'estado',
        'genero'
    ];




}
